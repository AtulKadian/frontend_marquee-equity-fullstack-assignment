import { useState } from "react";
import "./AddCompanies.css";
import { Autocomplete, TextField, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

const AddCompaies = () => {
  const navigate = useNavigate();
  let [startSearch, setStartSearch] = useState(false);
  let [suggestionsList, setSuggestionsList] = useState([]);
  let [selectedCompanyData, setSelectedCompanyData] = useState(null);

  const handleSelectedOption = (e, v) => {
    console.log(v);
    setSelectedCompanyData(v);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    let postOptions = {
      body: JSON.stringify({
        CIN: selectedCompanyData.CIN,
        companyName: selectedCompanyData.label,
      }),
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    fetch("http://localhost:5000/addNewCompany", postOptions)
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        console.log(result);
        navigate("/viewCompanies");
      })
      .catch((e) => console.log(e));
  };

  const updateSuggestions = (value) => {
    if (!value) {
      setStartSearch(false);
      setSuggestionsList([]);
      return;
    }
    setStartSearch(true);
    (async () => {
      let postOptions = {
        body: JSON.stringify({
          searchTerm: value,
        }),
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      let response = await fetch(
        "http://localhost:5000/fetchSuggestions",
        postOptions
      );
      let data = await response.json();
      setSuggestionsList(data.suggestions);
    })();
  };
  return (
    <div className="add-companies-container">
      <h2>Search and Add Companies</h2>
      <form className="search-form" onSubmit={(e) => handleSubmit(e)}>
        <Autocomplete
          disablePortal
          options={suggestionsList}
          loading={startSearch}
          loadingText="Searching..."
          sx={{ width: 400 }}
          onChange={(event, value) => handleSelectedOption(event, value)}
          renderInput={(params) => (
            <TextField
              required
              {...params}
              label="Company name"
              onChange={(e) => updateSuggestions(e.target.value)}
            />
          )}
        />
        &nbsp; &nbsp;
        <Button type="submit" size="large" variant="contained">
          Add
        </Button>
      </form>
    </div>
  );
};

export default AddCompaies;
