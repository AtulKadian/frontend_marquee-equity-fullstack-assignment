import "./ViewCompanies.css";
import { Button } from "@mui/material";
import { NavLink } from "react-router-dom";
import { useEffect, useState } from "react";

const ViewCompanies = () => {
  let [companiesData, setCompaniesData] = useState(null);

  useEffect(() => {
    fetch("http://localhost:5000/getAllCompanies")
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        setCompaniesData(result);
      })
      .catch((e) => console.log(e));
  }, []);
  if (!companiesData) {
    return;
  }
  return (
    <div className="view-companies-container">
      <div className="title">
        <h2>View Companies</h2>
      </div>
      <div className="main-content">
        <Button variant="outlined">
          <NavLink style={{ textDecoration: "none" }} to={"/"}>
            Add Companies
          </NavLink>
        </Button>
        <div className="table-container">
          <table className="table table-hover table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Company</th>
                <th scope="col">CIN</th>
              </tr>
            </thead>
            <tbody>
              {companiesData.result.map((row) => {
                return (
                  <tr key={row.company_id}>
                    <td>{row.company_id}</td>
                    <td>{row.company_name}</td>
                    <td>{row.company_cin}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default ViewCompanies;
