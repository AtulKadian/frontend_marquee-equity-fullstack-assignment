import "./App.css";
import AddCompaies from "./Pages/AddCompanies/AddCompanies";
import ViewCompaies from "./Pages/ViewCompanies/ViewCompanies";
import { BrowserRouter, Routes, Route } from "react-router-dom";

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<AddCompaies />} />
          <Route path="/viewCompanies" element={<ViewCompaies />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
